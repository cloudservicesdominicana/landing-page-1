﻿<!DOCTYPE html>
<html lang="es" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# product: http://ogp.me/ns/product#" itemscope="" itemtype="http://schema.org/Offer">
    <head>
        <link rel="shortcut icon" href="assets/v2/images/favicon.png">
        <meta charset="UTF-8">
        <!-- For IE -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- For Resposive Device -->
        <script data-ad-client="ca-pub-5303886503053125" async="" src="pagead/js/f.txt"></script>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title>Préstamos CLOUD</title>
			
			<!-- Facebook Pixel Code -->
			<script>
			!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
			n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
			n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
			t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
			document,'script','https://connect.facebook.net/en_US/fbevents.js');
			fbq('init', '1859642514270201' );			fbq('track', 'PageView');
			
			</script>
			<noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=1859642514270201&ev=PageView&noscript=1"></noscript>
			<!-- DO NOT MODIFY -->
			<!-- End Facebook Pixel Code -->
			
			
<!-- This site is optimized with the Yoast SEO plugin v13.0 - https://yoast.com/wordpress/plugins/seo/ -->
<meta name="robots" content="max-snippet:-1, max-image-preview:large, max-video-preview:-1">
<link rel="canonical" href="index.html">
<meta property="og:locale" content="es_ES">
<meta property="og:type" content="article">
<meta property="og:title" content="web-2 - Préstamos CLOUD">
<meta property="og:description" content="Sistema líder en Ventas &amp; Préstamos ¿Cansado de crear facturas a mano? ¡Aqui las herramientas más útiles todas en una, para cada área de su negocio, que lo impulsaran al próximo nivel! CREAR CUENTA ¡Descarga nuestra aplicación Para dispositivos móviles! El sistema PréstamosCLOUD es un sistema que le funcionara tanto para la gestion de sus &hellip;">
<meta property="og:url" content="https://app.prestamoscloud.com">
<meta property="og:site_name" content="Préstamos CLOUD">
<meta property="og:image" content="https://prestamoscloud.com/wp-content/uploads/2021/08/Dispositivo-Movil.png">
<meta property="og:image:secure_url" content="https://prestamoscloud.com/wp-content/uploads/2021/08/Dispositivo-Movil.png">
<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:description" content="Sistema líder en Ventas &amp; Préstamos ¿Cansado de crear facturas a mano? ¡Aqui las herramientas más útiles todas en una, para cada área de su negocio, que lo impulsaran al próximo nivel! CREAR CUENTA ¡Descarga nuestra aplicación Para dispositivos móviles! El sistema PréstamosCLOUD es un sistema que le funcionara tanto para la gestion de sus [&hellip;]">
<meta name="twitter:title" content="web-2 - Préstamos CLOUD">
<meta name="twitter:image" content="https://prestamoscloud.com/wp-content/uploads/2021/08/Dispositivo-Movil.png">
<script type='application/ld+json' class='yoast-schema-graph yoast-schema-graph--main'>{"@context":"https://schema.org","@graph":[{"@type":"Organization","@id":"https://prestamoscloud.com/#organization","name":"Pr\u00e9stamos CLOUD","url":"https://prestamoscloud.com/","sameAs":[],"logo":{"@type":"ImageObject","@id":"https://prestamoscloud.com/#logo","url":"https://prestamoscloud.com/wp-content/uploads/2020/02/logo_principal.png","width":1080,"height":459,"caption":"Pr\u00e9stamos CLOUD"},"image":{"@id":"https://prestamoscloud.com/#logo"}},{"@type":"WebSite","@id":"https://prestamoscloud.com/#website","url":"https://prestamoscloud.com/","name":"Pr\u00e9stamos CLOUD","description":"Tu gestor de pr\u00e9stamos en la nube","publisher":{"@id":"https://prestamoscloud.com/#organization"},"potentialAction":{"@type":"SearchAction","target":"https://prestamoscloud.com/?s={search_term_string}","query-input":"required name=search_term_string"}},{"@type":"ImageObject","@id":"https://prestamoscloud.com/index.php/web-2/#primaryimage","url":"https://prestamoscloud.com/wp-content/uploads/2021/08/Dispositivo-Movil.png","width":1073,"height":1264},{"@type":"WebPage","@id":"https://prestamoscloud.com/index.php/web-2/#webpage","url":"https://prestamoscloud.com/index.php/web-2/","inLanguage":"es","name":"web-2 - Pr\u00e9stamos CLOUD","isPartOf":{"@id":"https://prestamoscloud.com/#website"},"primaryImageOfPage":{"@id":"https://prestamoscloud.com/index.php/web-2/#primaryimage"},"datePublished":"2021-08-20T23:35:39+00:00","dateModified":"2021-08-20T23:35:50+00:00"}]}</script>
<!-- / Yoast SEO plugin. -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css" rel="stylesheet">

<link rel='dns-prefetch' href='index.html'>
<link rel='dns-prefetch' href='//fonts.googleapis.com'>
<link rel='dns-prefetch' href='index-1.htm'>
<link rel="alternate" type="application/rss+xml" title="Préstamos CLOUD &raquo; Feed" href="../feed/index.htm.rss">
<link rel="alternate" type="application/rss+xml" title="Préstamos CLOUD &raquo; Feed de los comentarios" href="../comments/feed/index.htm.rss">
		<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/72x72\/","ext":".png","svgUrl":"https:\/\/s.w.org\/images\/core\/emoji\/12.0.0-1\/svg\/","svgExt":".svg","source":{"concatemoji":"https:\/\/prestamoscloud.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=5.3.8"}};
			!function(e,a,t){var n,r,o,i=a.createElement("canvas"),p=i.getContext&&i.getContext("2d");function s(e,t){var a=String.fromCharCode;p.clearRect(0,0,i.width,i.height),p.fillText(a.apply(this,e),0,0);e=i.toDataURL();return p.clearRect(0,0,i.width,i.height),p.fillText(a.apply(this,t),0,0),e===i.toDataURL()}function c(e){var t=a.createElement("script");t.src=e,t.defer=t.type="text/javascript",a.getElementsByTagName("head")[0].appendChild(t)}for(o=Array("flag","emoji"),t.supports={everything:!0,everythingExceptFlag:!0},r=0;r<o.length;r++)t.supports[o[r]]=function(e){if(!p||!p.fillText)return!1;switch(p.textBaseline="top",p.font="600 32px Arial",e){case"flag":return s([127987,65039,8205,9895,65039],[127987,65039,8203,9895,65039])?!1:!s([55356,56826,55356,56819],[55356,56826,8203,55356,56819])&&!s([55356,57332,56128,56423,56128,56418,56128,56421,56128,56430,56128,56423,56128,56447],[55356,57332,8203,56128,56423,8203,56128,56418,8203,56128,56421,8203,56128,56430,8203,56128,56423,8203,56128,56447]);case"emoji":return!s([55357,56424,55356,57342,8205,55358,56605,8205,55357,56424,55356,57340],[55357,56424,55356,57342,8203,55358,56605,8203,55357,56424,55356,57340])}return!1}(o[r]),t.supports.everything=t.supports.everything&&t.supports[o[r]],"flag"!==o[r]&&(t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&t.supports[o[r]]);t.supports.everythingExceptFlag=t.supports.everythingExceptFlag&&!t.supports.flag,t.DOMReady=!1,t.readyCallback=function(){t.DOMReady=!0},t.supports.everything||(n=function(){t.readyCallback()},a.addEventListener?(a.addEventListener("DOMContentLoaded",n,!1),e.addEventListener("load",n,!1)):(e.attachEvent("onload",n),a.attachEvent("onreadystatechange",function(){"complete"===a.readyState&&t.readyCallback()})),(n=t.source||{}).concatemoji?c(n.concatemoji):n.wpemoji&&n.twemoji&&(c(n.twemoji),c(n.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
		<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}

.flex {
  display: flex;
  align-items: center;
  justify-content: center;
 }

 .app-btn {
   width: 45%;
   max-width: 160px;
   color: #fff;
   margin: 20px 10px;
   text-align: left;
   border-radius: 5px;
   text-decoration: none;
   font-family: "Lucida Grande", sans-serif;
   font-size: 10px;
   text-transform: uppercase;
    background-color: #101010;
    transition: 0.25s linear;
 }
 .app-btn:hover {
     background-color: #454545;
    }
   i {
    width: 20%;
    text-align: center;
    font-size: 28px;
    margin-right: 7px;
   }
   .big-txt {
    font-size: 17px;
    text-transform: capitalize;
   }
}
@media (max-width: 480px) {
  .mobile-hide {
    display: none;
  }
}

@media (min-width: 480px) {
  .mobile-hide {
    display: inline;
  }
  .mobile-show {
    display: none;
  }
}
 
</style>
	<link rel='stylesheet' id='wp-block-library-css' href='wp-includes/css/dist/block-library/style.min.css?ver=5.3.8' type='text/css' media='all'>
<link rel='stylesheet' id='wc-block-style-css' href='wp-content/plugins/woocommerce/packages/woocommerce-blocks/build/style.css?ver=2.5.11' type='text/css' media='all'>
<link rel='stylesheet' id='saasland-editor-fonts-css' href='css.css?family=Poppins%3A300%2C400%2C500%2C600%2C700%2C900&#038;subset' type='text/css' media='all'>
<link rel='stylesheet' id='font-awesome-5-free-css' href='wp-content/themes/saasland/assets/vendors/font-awesome/css/all.css?ver=5.3.8' type='text/css' media='all'>
<link rel='stylesheet' id='contact-form-7-css' href='wp-content/plugins/contact-form-7/includes/css/styles.css?ver=5.1.6' type='text/css' media='all'>
<link rel='stylesheet' id='rs-plugin-settings-css' href='wp-content/plugins/slider-revolution/public/assets/css/rs6.css?ver=6.1.8' type='text/css' media='all'>
<style id='rs-plugin-settings-inline-css' type='text/css'>
#rs-demo-id {}
</style>
<link rel='stylesheet' id='sb-main-css-css' href='wp-content/plugins/supportboard/include/main.css?ver=1.4' type='text/css' media='all'>
<link rel='stylesheet' id='sb-google-font-css' href='css-1.css?family=Raleway%3A500%2C600&#038;ver=1.0' type='text/css' media='all'>
<style id='woocommerce-inline-inline-css' type='text/css'>
.woocommerce form .form-row .required { visibility: visible; }
</style>
<link rel='stylesheet' id='wc-gateway-ppec-frontend-css' href='wp-content/plugins/woocommerce-gateway-paypal-express-checkout/assets/css/wc-gateway-ppec-frontend.css?ver=5.3.8' type='text/css' media='all'>
<link rel='stylesheet' id='tinvwl-css' href='wp-content/plugins/ti-woocommerce-wishlist/assets/css/public.min.css?ver=1.16.2' type='text/css' media='all'>
<link rel='stylesheet' id='saasland-fonts-css' href='css.css?family=Poppins%3A300%2C400%2C500%2C600%2C700%2C900&#038;subset' type='text/css' media='all'>
<link rel='stylesheet' id='bootstrap-css' href='wp-content/themes/saasland/assets/css/bootstrap.min.css?ver=5.3.8' type='text/css' media='all'>
<link rel='stylesheet' id='themify-icon-css' href='wp-content/themes/saasland/assets/vendors/themify-icon/themify-icons.css?ver=5.3.8' type='text/css' media='all'>
<link rel='stylesheet' id='elementor-frontend-css' href='wp-content/plugins/elementor/assets/css/frontend.min.css?ver=2.8.5' type='text/css' media='all'>
<link rel='stylesheet' id='saasland-elementor-css' href='wp-content/themes/saasland/assets/css/elementor-override.css?ver=5.3.8' type='text/css' media='all'>
<link rel='stylesheet' id='saasland-animate-css' href='wp-content/themes/saasland/assets/vendors/animation/animate.css?ver=5.3.8' type='text/css' media='all'>
<link rel='stylesheet' id='saasland-custom-animations-css' href='wp-content/themes/saasland/assets/css/saasland-animations.css?ver=5.3.8' type='text/css' media='all'>
<link rel='stylesheet' id='magnific-popup-css' href='wp-content/themes/saasland/assets/vendors/magnify-pop/magnific-popup.css?ver=5.3.8' type='text/css' media='all'>
<link rel='stylesheet' id='eleganticons-css' href='wp-content/themes/saasland/assets/vendors/elagent/style.css?ver=5.3.8' type='text/css' media='all'>
<link rel='stylesheet' id='saasland-wpd-css' href='wp-content/themes/saasland/assets/css/wpd-style.css?ver=5.3.8' type='text/css' media='all'>
<link rel='stylesheet' id='saasland-main-css' href='wp-content/themes/saasland/assets/css/style.css?ver=5.3.8' type='text/css' media='all'>
<link rel='stylesheet' id='saasland-elements-css' href='wp-content/themes/saasland/assets/css/elements.css?ver=5.3.8' type='text/css' media='all'>
<link rel='stylesheet' id='saasland-comments-css' href='wp-content/themes/saasland/assets/css/comments.css?ver=5.3.8' type='text/css' media='all'>
<link rel='stylesheet' id='saasland-preloader-css' href='wp-content/themes/saasland/assets/css/pre-loader.css?ver=5.3.8' type='text/css' media='all'>
<link rel='stylesheet' id='saasland-footer-css' href='wp-content/themes/saasland/assets/css/footer.css?ver=5.3.8' type='text/css' media='all'>
<link rel='stylesheet' id='saasland-gutenberg-css' href='wp-content/themes/saasland/assets/css/saasland-gutenberg.css?ver=5.3.8' type='text/css' media='all'>
<link rel='stylesheet' id='saasland-root-css' href='wp-content/themes/saasland/style.css?ver=5.3.8' type='text/css' media='all'>
<style id='saasland-root-inline-css' type='text/css'>

                .single-product .product_details_area,
                .single section.blog_area_two,
                .elementor-template-full-width .elementor.elementor-5309,
                .sec_pad.page_wrapper {
                    padding-top: px;
                }
            .single-post section.blog_area_two,
            .elementor-template-full-width .elementor.elementor-5309,
            .sec_pad.page_wrapper {
                padding-bottom: px;
            } 
                .new_footer_top .footer_bg {
                    background: url(wp/saasland-theme/wp-content/themes/saasland/assets/img/seo/footer_bg.png ) no-repeat scroll center 0;
                }
                .new_footer_top .footer_bg .footer_bg_one {
                    background: url(wp-content/uploads/2020/02/car.png ) no-repeat center center;
                }
</style>
<link rel='stylesheet' id='saasland-responsive-css' href='wp-content/themes/saasland/assets/css/responsive.css?ver=5.3.8' type='text/css' media='all'>
<link rel='stylesheet' id='saasland-responsive2-css' href='wp-content/themes/saasland/assets/css/responsive-2.css?ver=5.3.8' type='text/css' media='all'>
<link rel='stylesheet' id='saasland-shop-css' href='wp-content/themes/saasland/assets/css/shop.css?ver=5.3.8' type='text/css' media='all'>
<link rel='stylesheet' id='mCustomScrollbar-css' href='wp-content/themes/saasland/assets/vendors/scroll/jquery.mCustomScrollbar.min.css?ver=5.3.8' type='text/css' media='all'>
<link rel='stylesheet' id='elementor-icons-css' href='wp-content/plugins/elementor/assets/lib/eicons/css/elementor-icons.min.css?ver=5.5.0' type='text/css' media='all'>
<link rel='stylesheet' id='simple-line-icon-css' href='wp-content/plugins/saasland-core/assets/vendors/simple-line-icon/simple-line-icons.css?ver=5.3.8' type='text/css' media='all'>
<link rel='stylesheet' id='slick-theme-css' href='wp-content/plugins/saasland-core/assets/vendors/slick/slick-theme.css?ver=5.3.8' type='text/css' media='all'>
<link rel='stylesheet' id='themify-icons-css' href='wp-content/plugins/saasland-core/assets/vendors/themify-icon/themify-icons.css?ver=5.3.8' type='text/css' media='all'>
<link rel='stylesheet' id='saasland-flaticons-css' href='wp-content/plugins/saasland-core/assets/vendors/flaticon/flaticon.css?ver=5.3.8' type='text/css' media='all'>
<link rel='stylesheet' id='elementor-post-5309-css' href='wp-content/uploads/elementor/css/post-5309.css?ver=1629502554' type='text/css' media='all'>
<link rel='stylesheet' id='elementor-icons-shared-0-css' href='wp-content/plugins/elementor/assets/lib/font-awesome/css/fontawesome.min.css?ver=5.9.0' type='text/css' media='all'>
<link rel='stylesheet' id='elementor-icons-fa-solid-css' href='wp-content/plugins/elementor/assets/lib/font-awesome/css/solid.min.css?ver=5.9.0' type='text/css' media='all'>
<script type='text/javascript' src='wp-includes/js/jquery/jquery.js?ver=1.12.4-wp'></script>
<script type='text/javascript' src='wp-includes/js/jquery/jquery-migrate.min.js?ver=1.4.1'></script>
<script type='text/javascript' src='wp-content/plugins/slider-revolution/public/assets/js/revolution.tools.min.js?ver=6.0'></script>
<script type='text/javascript' src='wp-content/plugins/slider-revolution/public/assets/js/rs6.min.js?ver=6.1.8'></script>
<script type='text/javascript' src='wp-content/plugins/supportboard/include/main.js?ver=1.4'></script>
<script type='text/javascript'>
var sb_ajax_url = 'https://prestamoscloud.com/wp-admin/admin-ajax.php'; var sb_wp_url = 'https://prestamoscloud.com';
var sb_plugin_url_wp = 'https://prestamoscloud.com/wp-content/plugins/supportboard';
</script>
<link rel='https://api.w.org/' href='../wp-json/index.htm.json'>
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="xmlrpc.php.xml?rsd">
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="wp-includes/wlwmanifest.xml"> 
<meta name="generator" content="WordPress 5.3.8">
<meta name="generator" content="WooCommerce 3.9.1">
<link rel='shortlink' href='index.htm?p=5309'>
<link rel="alternate" type="application/json+oembed" href="../wp-json/oembed/1.0/embed.json?url=https%3A%2F%2Fprestamoscloud.com%2Findex.php%2Fweb-2%2F">
<link rel="alternate" type="text/xml+oembed" href="../wp-json/oembed/1.0/embed.xml?url=https%3A%2F%2Fprestamoscloud.com%2Findex.php%2Fweb-2%2F&#038;format=xml">

		<!-- GA Google Analytics @ https://m0n.co/ga -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=G-KCET8ZKT04"></script>
		<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());

		gtag('config', 'G-KCET8ZKT04');
		</script>

	<meta name="referrer" content="always">	<noscript><style>.woocommerce-product-gallery{ opacity: 1 !important; }</style></noscript>
	<style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style>		<style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style>
		<meta name="generator" content="Powered by Slider Revolution 6.1.8 - responsive, Mobile-Friendly Slider Plugin for WordPress with comfortable drag and drop interface.">
<script type="text/javascript">function setREVStartSize(e){			
			try {								
				var pw = document.getElementById(e.c).parentNode.offsetWidth,
					newh;
				pw = pw===0 || isNaN(pw) ? window.innerWidth : pw;
				e.tabw = e.tabw===undefined ? 0 : parseInt(e.tabw);
				e.thumbw = e.thumbw===undefined ? 0 : parseInt(e.thumbw);
				e.tabh = e.tabh===undefined ? 0 : parseInt(e.tabh);
				e.thumbh = e.thumbh===undefined ? 0 : parseInt(e.thumbh);
				e.tabhide = e.tabhide===undefined ? 0 : parseInt(e.tabhide);
				e.thumbhide = e.thumbhide===undefined ? 0 : parseInt(e.thumbhide);
				e.mh = e.mh===undefined || e.mh=="" || e.mh==="auto" ? 0 : parseInt(e.mh,0);		
				if(e.layout==="fullscreen" || e.l==="fullscreen") 						
					newh = Math.max(e.mh,window.innerHeight);				
				else{					
					e.gw = Array.isArray(e.gw) ? e.gw : [e.gw];
					for (var i in e.rl) if (e.gw[i]===undefined || e.gw[i]===0) e.gw[i] = e.gw[i-1];					
					e.gh = e.el===undefined || e.el==="" || (Array.isArray(e.el) && e.el.length==0)? e.gh : e.el;
					e.gh = Array.isArray(e.gh) ? e.gh : [e.gh];
					for (var i in e.rl) if (e.gh[i]===undefined || e.gh[i]===0) e.gh[i] = e.gh[i-1];
										
					var nl = new Array(e.rl.length),
						ix = 0,						
						sl;					
					e.tabw = e.tabhide>=pw ? 0 : e.tabw;
					e.thumbw = e.thumbhide>=pw ? 0 : e.thumbw;
					e.tabh = e.tabhide>=pw ? 0 : e.tabh;
					e.thumbh = e.thumbhide>=pw ? 0 : e.thumbh;					
					for (var i in e.rl) nl[i] = e.rl[i]<window.innerWidth ? 0 : e.rl[i];
					sl = nl[0];									
					for (var i in nl) if (sl>nl[i] && nl[i]>0) { sl = nl[i]; ix=i;}															
					var m = pw>(e.gw[ix]+e.tabw+e.thumbw) ? 1 : (pw-(e.tabw+e.thumbw)) / (e.gw[ix]);					

					newh =  (e.type==="carousel" && e.justify==="true" ? e.gh[ix] : (e.gh[ix] * m)) + (e.tabh + e.thumbh);
				}			
				
				if(window.rs_init_css===undefined) window.rs_init_css = document.head.appendChild(document.createElement("style"));					
				document.getElementById(e.c).height = newh;
				window.rs_init_css.innerHTML += "#"+e.c+"_wrapper { height: "+newh+"px }";				
			} catch(e){
				console.log("Failure at Presize of Slider:" + e)
			}					   
		  };</script>
		<style type="text/css" id="wp-custom-css">
			
				.elementor-element.elementor-element-b8efe90.elementor-widget.elementor-widget-Saasland_testimonial_single {
    position: static;
}

.hover-blue .elementor-text-editor a:hover {
    color: #00aff0 !important;
}

.hover-white .elementor-text-editor a:hover {
    color: #fff !important;
}

.page-id-529 .menu_toggle .hamburger span, .page-id-529 .menu_toggle .hamburger-cross span {
    background: #fff !important;
}		

.breadcrumb_area{
	display:none;
}		</style>
		<style type="text/css" title="dynamic-css" class="options-output">.navbar-brand>img{height:28px;width:202px;}.header_area .navbar .btn_get{color:#1e73be;}.header_area .navbar .btn_get{border-color:#ffffff;}.header_area .navbar .btn_get{background:#ffffff;}.header_area .navbar .btn_get:hover{color:#ffffff;}.header_area .navbar .btn_get:hover{border-color:transparent;}.header_area .navbar .btn_get:hover{background:#1e73be;}.navbar_fixed .header_area .navbar .btn_get:hover{border-color:#1e73be;}.navbar_fixed.header_area .navbar .btn_get{border-color:#ffffff;}.navbar_fixed.header_area .navbar .btn_get{color:#000000;}.navbar_fixed.header_area .navbar .btn_get{background:transparent;}.header_area.navbar_fixed .navbar .btn_get.btn-meta:hover{color:#ffffff;}.header_area.navbar_fixed .navbar .btn_get.btn-meta:hover{background:#1e73be;}.header_area.navbar_fixed .navbar .btn_get.btn-meta:hover{border-color:transparent;}.header_area .navbar .navbar-nav .menu-item a{color:#ffffff;}.menu_toggle .hamburger span, .menu_toggle .hamburger-cross span, .navbar .search_cart .search a.nav-link:before{background:#ffffff;}.header_area .navbar .navbar-nav .menu-item a:hover, .header_area .menu > .nav-item.active .nav-link{color:#ffffff;}.header_area .navbar .navbar-nav .menu-item{margin-top:0;margin-right:40px;margin-bottom:0;margin-left:0;}header.navbar_fixed .menu > .nav-item > .nav-link, header.header_area.navbar_fixed .navbar .navbar-nav .menu-item a, 
                               .header_area.navbar_fixed .menu_center .menu > .nav-item > .nav-link, header.navbar_fixed .navbar .search_cart .search a.nav-link i{color:#051441;}header.navbar_fixed .menu_toggle .hamburger span, .menu_toggle .hamburger-cross span,
                                 header.header_area.navbar_fixed .menu_toggle .hamburger-cross span, header.header_area.navbar_fixed .menu_toggle .hamburger span, 
                                 header.navbar_fixed .navbar .search_cart .search a.nav-link:before{background:#051441;}
                    .header_area .navbar_fixed .navbar .navbar-nav .menu-item a:hover, 
                    .header_area .navbar_fixed .menu > .nav-item.active .nav-link,
                    .header_area.navbar_fixed .menu_center .menu > .nav-item:hover > .nav-link,
                    .menu_center .menu > .nav-item.submenu .dropdown-menu .nav-item:hover > .nav-link span.arrow_carrot-right,
                    .menu_center .menu > .nav-item.submenu .dropdown-menu .nav-item.active > .nav-link, 
                    .menu_center .menu > .nav-item.submenu .dropdown-menu .nav-item:hover > .nav-link,
                    .header_area.navbar_fixed .menu_center .menu > .nav-item.active > .nav-link
                {color:#5e2ced;}.blog .header_area .navbar .navbar-nav .menu-item a{color:#051441;}</style>    </head>

    <body class="page-template page-template-elementor_header_footer page page-id-5309 wp-embed-responsive theme-saasland woocommerce-no-js tinvwl-theme-style not_logged_in elementor-default elementor-template-full-width elementor-page elementor-page-5309" data-spy="scroll" data-target=".navbar" data-offset="70">
    
    
<div id="preloader">
    <div id="ctn-preloader" class="ctn-preloader">
        <div class="animation-preloader">
            <div class="spinner"></div>
            <div class="txt-loading">
                                        <span data-text-preloader="C" class="letters-loading">
                            C                        </span>
                                                <span data-text-preloader="A" class="letters-loading">
                            A                        </span>
                                                <span data-text-preloader="R" class="letters-loading">
                            R                        </span>
                                                <span data-text-preloader="G" class="letters-loading">
                            G                        </span>
                                                <span data-text-preloader="A" class="letters-loading">
                            A                        </span>
                                                <span data-text-preloader="N" class="letters-loading">
                            N                        </span>
                                                <span data-text-preloader="D" class="letters-loading">
                            D                        </span>
                                                <span data-text-preloader="O" class="letters-loading">
                            O                        </span>
                                    </div>
                            <p class="text-center"> Prestamos CLOUD </p>
                    </div>
        <div class="loader">
            <div class="row">
                <div class="col-lg-3 loader-section section-left"><div class="bg"></div></div>
                <div class="col-lg-3 loader-section section-left"><div class="bg"></div></div>
                <div class="col-lg-3 loader-section section-right"><div class="bg"></div></div>
                <div class="col-lg-3 loader-section section-right"><div class="bg"></div></div>
            </div>
        </div>
    </div>
</div>
<div class="body_wrapper">

    <header class="header_area  header_stick">
        
        <nav class="navbar navbar-expand-lg menu_center">
            <div class="container">            <a class="navbar-brand sticky_logo" href="#">
                                    <img src="wp-content/uploads/2020/02/Desktop-Header-Logo-White-PrestamosCloud.png" srcset='wp-content/uploads/2020/02/Desktop-Header-Logo-White-PrestamosCloud.png 2x' alt="Préstamos CLOUD">
                    <img src="wp-content/uploads/2020/02/Desktop-Header-Logo-Color-PrestamosCloud.png" srcset='wp-content/uploads/2020/02/Desktop-Header-Logo-Color-PrestamosCloud.png 2x' alt="Préstamos CLOUD">
                                </a>

            
                            <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="menu_toggle">
                                <span class="hamburger">
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                </span>
                                <span class="hamburger-cross">
                                    <span></span>
                                    <span></span>
                                </span>
                            </span>
                </button>
            
            <div class="collapse navbar-collapse justify-content-center" id="navbarSupportedContent">
                
    <a class="menu_cus btn_get btn-meta btn_get_radious" href="https://oficina.prestamoscloud.com/login">
        Ir a mi oficina    </a>
            </div>

                <div class="alter_nav ">
        <ul class="navbar-nav search_cart menu">

            
                    </ul>
    </div></div>        </nav>
    </header>
    
    <section class="breadcrumb_area center">
    <img src='wp/saasland-theme/wp-content/themes/saasland/assets/img/banners/banner_bg.png' class='breadcrumb_shap' alt='web-2'>    <div class="container">
        <div class="breadcrumb_content text-center">
            <h1 class="f_p f_700 f_size_50 w_color l_height50 mb_20">
                web-2            </h1>
                    </div>
    </div>
</section>		<div data-elementor-type="wp-page" data-elementor-id="5309" class="elementor elementor-5309" data-elementor-settings="[]">
			<div class="elementor-inner">
				<div class="elementor-section-wrap">
							<section class="elementor-element elementor-element-7e8418dc elementor-section-full_width elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id="7e8418dc" data-element_type="section">
						<div class="elementor-container elementor-column-gap-no">
				<div class="elementor-row">
				<div class="elementor-element elementor-element-39bba821 elementor-column elementor-col-100 elementor-top-column" data-id="39bba821" data-element_type="column">
			<div class="elementor-column-wrap  elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div class="elementor-element elementor-element-1b1a3907 elementor-widget elementor-widget-saasland_hero" data-id="1b1a3907" data-element_type="widget" data-widget_type="saasland_hero.default">
				<div class="elementor-widget-container">
			<section class="hosting_banner_area">
    <ul class="list-unstyled b_line">
                                                                    </ul>
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-7 d-flex align-items-center">
                <div class="hosting_content">
                                        <h2 class="hosting_color_s wow fadeInUp" style="margin-top: 30%;" data-wow-delay="0.3s">Sistema líder en <br>
<span style="color: yellow">Ventas &amp; Préstamos</span></h2>
                                                    <p class="wow fadeInUp" data-wow-delay="0.5s">¿Cansado de crear facturas a mano? <br>
<small>¡Aqui las herramientas más útiles todas en una, para cada<br>
 área de su negocio, que lo impulsaran al próximo nivel!</small></p>
                
                                                                    <a href="https://oficina.prestamoscloud.com/registro/nuevo" class="hosting_btn btn_hover wow fadeInUp elementor-repeater-item-fd5f9b2" data-wow-delay="0.7s">
                                CREAR CUENTA                            </a>
                                                            </div>
        </div>
        <div class="col-lg-6 col-md-5">
            <img class="img-fluid wow fadeInRight" data-wow-delay="0.7s" src="wp-content/uploads/2021/08/Dispositivo-Movil.png" alt="Sistema líder en 
&lt;span style=&quot;color: yellow;&quot;&gt;Ventas &amp; Préstamos&lt;/span&gt;">
        </div>
    </div>
    </div>
</section>		</div>
				</div>
						</div>
			</div>
		</div>
						</div>
			</div>
		</section>
				<section class="elementor-element elementor-element-2776b9dd elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id="2776b9dd" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
				<div class="elementor-row">
				<div class="elementor-element elementor-element-e8edfab elementor-column elementor-col-100 elementor-top-column" data-id="e8edfab" data-element_type="column">
			<div class="elementor-column-wrap  elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div class="elementor-element elementor-element-265f99fb elementor-hidden-tablet elementor-hidden-phone elementor-widget elementor-widget-image" data-id="265f99fb" data-element_type="widget" data-widget_type="image.default">
				<div class="elementor-widget-container">
					<div class="elementor-image">
										<img width="1920" height="331" src="wp-content/uploads/2021/08/Descarga-nuestra-app-Background.png" class="attachment-full size-full" alt="" srcset="wp-content/uploads/2021/08/Descarga-nuestra-app-Background.png 1920w, wp-content/uploads/2021/08/Descarga-nuestra-app-Background-300x52.png 300w, wp-content/uploads/2021/08/Descarga-nuestra-app-Background-1024x177.png 1024w, wp-content/uploads/2021/08/Descarga-nuestra-app-Background-768x132.png 768w, wp-content/uploads/2021/08/Descarga-nuestra-app-Background-1536x265.png 1536w, wp-content/uploads/2021/08/Descarga-nuestra-app-Background-600x103.png 600w" sizes="(max-width: 1920px) 100vw, 1920px">											</div>
				</div>
				</div>
						</div>
			</div>
		</div>
						</div>
			</div>
		</section>
				<section class="elementor-element elementor-element-1e87d9b elementor-section-full_width elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id="1e87d9b" data-element_type="section">
						<div class="elementor-container elementor-column-gap-no">
				<div class="elementor-row">
				<div class="elementor-element elementor-element-6e2e88b2 elementor-column elementor-col-100 elementor-top-column" data-id="6e2e88b2" data-element_type="column">
			<div class="elementor-column-wrap  elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div class="elementor-element elementor-element-4ab3ced9 elementor-hidden-tablet elementor-hidden-phone elementor-widget elementor-widget-heading" data-id="4ab3ced9" data-element_type="widget" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<span class="elementor-heading-title elementor-size-xl"><b style="text-align: center;">¡Descarga nuestra aplicación </b><br>Para dispositivos móviles!</span>		</div>
				</div>
				<div class="elementor-element elementor-element-3a266f50 elementor-hidden-tablet elementor-hidden-phone elementor-widget elementor-widget-image" data-id="3a266f50" data-element_type="widget" data-widget_type="image.default">
				<div class="elementor-widget-container">
					<div class="elementor-image">
										<img width="395" height="66" src="wp-content/uploads/2021/08/iphone-y-android.png" class="attachment-large size-large" alt="" srcset="wp-content/uploads/2021/08/iphone-y-android.png 395w, wp-content/uploads/2021/08/iphone-y-android-300x50.png 300w" sizes="(max-width: 395px) 100vw, 395px">											</div>
				</div>
				</div>
						</div>
			</div>
		</div>
						</div>
			</div>
		</section>
				<section class="elementor-element elementor-element-72ad9e31 elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id="72ad9e31" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
				<div class="elementor-row">
				<div class="elementor-element elementor-element-143b36b9 elementor-column elementor-col-100 elementor-top-column" data-id="143b36b9" data-element_type="column">
			<div class="elementor-column-wrap  elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div class="elementor-element elementor-element-7e32fa1 elementor-widget elementor-widget-saasland_hero" data-id="7e32fa1" data-element_type="widget" data-widget_type="saasland_hero.default">
				<div class="elementor-widget-container">
			<section class="hosting_banner_area">
    <ul class="list-unstyled b_line">
                                                                    </ul>
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-7 d-flex align-items-center">
                <div class="hosting_content">
                                        <h1 class="hosting_color_s wow fadeInUp" data-wow-delay="0.3s"><b style="color:#4058b9">El sistema<br>
</b></h1>
                                                    <p class="wow fadeInUp" data-wow-delay="0.5s"><span style="color:#4058b9"><br>
<br>
<b>PréstamosCLOUD </b>es un sistema que le funcionara tanto para la gestion de sus ventas como para la creación de prestamos. Le permitirá manejar de forma rápida, fácil y segura sus productos, clientes, compras, gastos y sus préstamos ofreciendo una serie de herramientas que llevará su negocio a cada vez necesitar menos personal administrativo, ya que el sistema lo hará todo, mucho mas fácil, rápido y mas económico que adquiriendo otro sistema costoso y anticuado.<br>
<br>
<br>
</span></p>
                
                                                                    <a href="https://oficina.prestamoscloud.com/registro/nuevo" class="hosting_btn btn_hover wow fadeInUp elementor-repeater-item-6e14701" data-wow-delay="0.7s">
                                Crear cuenta                            </a>
                                                            </div>
        </div>
        <div class="col-lg-6 col-md-5">
            <img class="img-fluid wow fadeInRight" data-wow-delay="0.7s" src="wp-content/uploads/2021/08/Dispositivo-Laptop.png" alt="&lt;b style=&quot;color:#4058b9;&quot;&gt;El sistema
&lt;/b&gt;">
        </div>
    </div>
    </div>
</section>		</div>
				</div>
						</div>
			</div>
		</div>
						</div>
			</div>
		</section>
				<section class="elementor-element elementor-element-1470cc8e elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id="1470cc8e" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
						<div class="elementor-container elementor-column-gap-default">
				<div class="elementor-row">
				<div class="elementor-element elementor-element-3e03454b elementor-column elementor-col-100 elementor-top-column" data-id="3e03454b" data-element_type="column">
			<div class="elementor-column-wrap  elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div class="elementor-element elementor-element-35738844 elementor-widget elementor-widget-saasland_main_features" data-id="35738844" data-element_type="widget" data-widget_type="saasland_main_features.default">
				<div class="elementor-widget-container">
			            <section class="software_featured_area has_subtitle">
                <div class="container">
                                        <h2 class="f_600 f_size_30 t_color3 text-center l_height40 mb_70 wow fadeInUp" data-wow-delay="0.3s">
                    Funcionalidades                </h2>
                                                    <p class="f_300 f_size_16 wow fadeInUp" data-wow-delay="0.4s">
                        <b>Estas son algunas de las funcionalidades.<br>
</b>Pero ojo, aquí no estarán todas, ya que somos los únicos en crear y subir a la plataforma módulos nuevos disponibles PARA TODOS, sin ningún costo extra.<br>
<br>
                    </p>
                                <div class="row software_featured_info">
                                                <div class="col-lg-3 col-sm-6 wow fadeInUp" data-wow-delay="0.3s">
                                <div class="software_featured_item text-center mb_20">
                                    <div class="s_icon">
                                                                                    <img src="wp-content/uploads/2021/08/Manejo-de-préstamos.png" alt="">
                                                                                                                    </div>
                                                                                                                                            </div>
                            </div>
                                                        <div class="col-lg-3 col-sm-6 wow fadeInUp" data-wow-delay="0.5s">
                                <div class="software_featured_item text-center mb_20">
                                    <div class="s_icon">
                                                                                    <img src="wp-content/uploads/2021/08/Punto-de-ventas.png" alt="">
                                                                                                                    </div>
                                                                                                                                            </div>
                            </div>
                                                        <div class="col-lg-3 col-sm-6 wow fadeInUp" data-wow-delay="0.7s">
                                <div class="software_featured_item text-center mb_20">
                                    <div class="s_icon">
                                                                                    <img src="wp-content/uploads/2021/08/Formulario-de-solicitud.png" alt="">
                                                                                                                    </div>
                                                                                                                                            </div>
                            </div>
                                                        <div class="col-lg-3 col-sm-6 wow fadeInUp" data-wow-delay="0.9s">
                                <div class="software_featured_item text-center mb_20">
                                    <div class="s_icon">
                                                                                    <img src="wp-content/uploads/2021/08/Generador-de-doc..png" alt="">
                                                                                                                    </div>
                                                                                                                                            </div>
                            </div>
                                                        <div class="col-lg-3 col-sm-6 wow fadeInUp" data-wow-delay="1.1s">
                                <div class="software_featured_item text-center mb_20">
                                    <div class="s_icon">
                                                                                    <img src="wp-content/uploads/2021/08/Cuentas-por-cobrar.png" alt="">
                                                                                                                    </div>
                                                                                                                                            </div>
                            </div>
                                                        <div class="col-lg-3 col-sm-6 wow fadeInUp" data-wow-delay="1.3s">
                                <div class="software_featured_item text-center mb_20">
                                    <div class="s_icon">
                                                                                    <img src="wp-content/uploads/2021/08/Abre-nuevas-ventas.png" alt="">
                                                                                                                    </div>
                                                                                                                                            </div>
                            </div>
                                                        <div class="col-lg-3 col-sm-6 wow fadeInUp" data-wow-delay="1.5s">
                                <div class="software_featured_item text-center mb_20">
                                    <div class="s_icon">
                                                                                    <img src="wp-content/uploads/2021/08/Registra-tus-productos.png" alt="">
                                                                                                                    </div>
                                                                                                                                            </div>
                            </div>
                                                        <div class="col-lg-3 col-sm-6 wow fadeInUp" data-wow-delay="1.7s">
                                <div class="software_featured_item text-center mb_20">
                                    <div class="s_icon">
                                                                                    <img src="wp-content/uploads/2021/08/Rutas-Geolocalización.png" alt="">
                                                                                                                    </div>
                                                                                                                                            </div>
                            </div>
                                            </div>
                </div>
            </section>
        		</div>
				</div>
				<div class="elementor-element elementor-element-576f34ab elementor-align-center elementor-mobile-align-center elementor-widget elementor-widget-button" data-id="576f34ab" data-element_type="widget" data-widget_type="button.default">
				<div class="elementor-widget-container">
					<div class="elementor-button-wrapper">
			<a href="https://oficina.prestamoscloud.com/registro/nuevo" class="elementor-button-link elementor-button elementor-size-sm" role="button">
						<span class="elementor-button-content-wrapper">
						<span class="elementor-button-text">Crear cuenta</span>
		</span>
					</a>
		</div>
				</div>
				</div>
						</div>
			</div>
		</div>
						</div>
			</div>
		</section>
				<section class="elementor-element elementor-element-a540967 elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id="a540967" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
				<div class="elementor-row">
				<div class="elementor-element elementor-element-6583e872 elementor-column elementor-col-100 elementor-top-column" data-id="6583e872" data-element_type="column">
			<div class="elementor-column-wrap  elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div class="elementor-element elementor-element-6c149af2 elementor-widget elementor-widget-heading" data-id="6c149af2" data-element_type="widget" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h1 class="elementor-heading-title elementor-size-xl">Precios</h1>		</div>
				</div>
						</div>
			</div>
		</div>
						</div>
			</div>
		</section>
				<section class="elementor-element elementor-element-36489ea elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id="36489ea" data-element_type="section">
						<div class="elementor-container elementor-column-gap-default">
				<div class="elementor-row">
				<div class="elementor-element elementor-element-1f6fcda8 elementor-column elementor-col-100 elementor-top-column" data-id="1f6fcda8" data-element_type="column">
			<div class="elementor-column-wrap  elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div class="elementor-element elementor-element-37b91800 elementor-widget elementor-widget-saasland-pricing-table-tabs-carousel" data-id="37b91800" data-element_type="widget" data-widget_type="saasland-pricing-table-tabs-carousel.default">
				<div class="elementor-widget-container">
			        <div class="container custom_container p0">

            <ul class="nav nav-tabs price_tab mt_70" id="myTab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active active_hover" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">
                        Pago mensual                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">
                        Pago anual                    </a>
                </li>
            </ul>
            <div class="tab-content price_content" id="myTabContent">
                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                    <div class="row test_slicKSlider">

                                                <div class="price_item">
                                
                                                                                                    <h5 class="f_p f_size_20 f_600 t_color2 mt_30"> Emprendedor </h5>
                                
                            <p>Aspirantes a grandes empresarios</p>

                            <div class="price f_700 f_size_40 t_color2">
                                $20                                                                    <sub class="f_size_16 f_300"> / Por mes </sub>
                                                            </div>

                            <ul class="list-unstyled p_list">
                                <li>Aplicación móvil</li>
<li>Acceso a 2 usuarios</li>
<li>Límite de 500 préstamos</li>
<li>Límite de 500 clientes </li>
<li>Ventas ilimitadas</li>
<li>200 whatsapp BOT</li>
<li>Soporte via whatsapp</li>
<li>Backup diario</li>                            </ul>

                                                            <a href="https://oficina.prestamoscloud.com/registro/nuevo" class="price_btn btn_hover">
                                    Probar por 10 dias                                </a>
                                                    </div>
                                                <div class="price_item">
                                                                    <div class="tag"><span> Popular </span></div>
                                
                                                                                                    <h5 class="f_p f_size_20 f_600 t_color2 mt_30"> Bronce </h5>
                                
                            <p>Ideal para pequeñas empresas</p>

                            <div class="price f_700 f_size_40 t_color2">
                                $30                                                                    <sub class="f_size_16 f_300"> / Por mes </sub>
                                                            </div>

                            <ul class="list-unstyled p_list">
                                <li><span>Aplicación</span> móvil</li>
<li>Acceso a 3 usuarios</li>
<li>(5 usd por cada usuario)</li>   
<li>Límite de 3000 préstamos</li>
<li>Límite de 3000 clientes </li>
<li>Ventas ilimitadas</li>
<li>300 whatsapp BOT</li>
<li>Soporte via whatsapp</li>
<li>Backup diario</li>
                            </ul>

                                                            <a href="https://oficina.prestamoscloud.com/registro/nuevo" class="price_btn btn_hover">
                                    Probar por 10 dias                                </a>
                                                    </div>
                                                <div class="price_item">
                                
                                                                                                    <h5 class="f_p f_size_20 f_600 t_color2 mt_30"> Plata </h5>
                                
                            <p>Empresas en crecimiento</p>

                            <div class="price f_700 f_size_40 t_color2">
                                $50                                                                    <sub class="f_size_16 f_300"> / Por mes </sub>
                                                            </div>

                            <ul class="list-unstyled p_list">
                                <li>Aplicación móvil</li>
<li>Acceso a 8 usuarios</li>
<li>(5 usd por cada usuario)</li>   
<li>Límite de 6000 préstamos</li>
<li>Límite de 6000 clientes </li>
<li>Ventas ilimitadas</li>
<li>ilimitado whatsapp BOT</li>
<li>Soporte via whatsapp</li>
<li>Backup diario</li>
                            </ul>

                                                            <a href="https://oficina.prestamoscloud.com/registro/nuevo" class="price_btn btn_hover">
                                    Probar por 10 dias                                </a>
                                                    </div>
                                                <div class="price_item">
                                
                                                                                                    <h5 class="f_p f_size_20 f_600 t_color2 mt_30"> Oro </h5>
                                
                            <p>Plan empresarial y cooperativas</p>

                            <div class="price f_700 f_size_40 t_color2">
                                $90                                                                    <sub class="f_size_16 f_300"> / Por mes </sub>
                                                            </div>

                            <ul class="list-unstyled p_list">
                                <li>Aplicación móvil</li>
<li>Acceso a 15 usuarios</li>
<li>(5 usd por cada usuario)</li>   
<li>Sin límite de préstamos</li>
<li>Sin límite de clientes </li>
<li>Ventas ilimitadas</li>
<li>ilimitado whatsapp BOT</li>
<li>Soporte via whatsapp</li>
<li>Backup diario</li>
<li>Modificaciones al sistema personalizadas</li>
                            </ul>

                                                            <a href="https://oficina.prestamoscloud.com/registro/nuevo" class="price_btn btn_hover">
                                    Probar por 10 dias                                </a>
                                                    </div>
                                        </div>
            </div>
            <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                <div class="row test_slicKSlider">
                                        <div class="price_item">

                            
                            
                                                        <h5 class="f_p f_size_20 f_600 t_color2 mt_30"> Emprendedor </h5>
                        
                        <p>Aspirantes a grandes empresarios</p>

                        <div class="price f_700 f_size_40 t_color2">
                            $240                                                            <sub class="f_size_16 f_300"> / Anualmente </sub>
                                                    </div>

                        <ul class="list-unstyled p_list">
                            <li>Aplicación móvil</li>
<li>Acceso a 2 usuarios</li>
<li>Límite de 500 préstamos</li>
<li>Límite de 500 clientes </li>
<li>Ventas ilimitadas</li>
<li>200 whatsapp BOT</li>
<li>Soporte via whatsapp</li>
<li>Backup diario</li>                        </ul>

                                                    <a href="https://oficina.prestamoscloud.com/registro/nuevo" class="price_btn btn_hover">
                                Probar por 10 dias                            </a>
                                            </div>
                                    <div class="price_item">

                                                            <div class="tag"><span> popular </span></div>
                            
                            
                                                        <h5 class="f_p f_size_20 f_600 t_color2 mt_30"> Bronce </h5>
                        
                        <p>Ideal para pequeñas empresas</p>

                        <div class="price f_700 f_size_40 t_color2">
                            325                                                            <sub class="f_size_16 f_300"> / Anualmente </sub>
                                                    </div>

                        <ul class="list-unstyled p_list">
                            <li>Aplicación móvil</li>
<li>Acceso a 3 usuarios</li>
<li>(5 usd por cada usuario)</li>   
<li>Límite de 3000 préstamos</li>
<li>Límite de 3000 clientes </li>
<li>Ventas ilimitadas</li>
<li>300 whatsapp BOT</li>
<li>Soporte via whatsapp</li>
<li>Backup diario</li>
                        </ul>

                                                    <a href="https://oficina.prestamoscloud.com/registro/nuevo" class="price_btn btn_hover">
                                Probar por 10 dias                            </a>
                                            </div>
                                    <div class="price_item">

                            
                            
                                                        <h5 class="f_p f_size_20 f_600 t_color2 mt_30"> Plata </h5>
                        
                        <p>Empresas en crecimiento</p>

                        <div class="price f_700 f_size_40 t_color2">
                            $540                                                            <sub class="f_size_16 f_300"> / Anualmente </sub>
                                                    </div>

                        <ul class="list-unstyled p_list">
                            <li>Aplicación móvil</li>
<li>Acceso a 8 usuarios</li>
<li>(5 usd por cada usuario)</li>   
<li>Límite de 6000 préstamos</li>
<li>Límite de 6000 clientes </li>
<li>Ventas ilimitadas</li>
<li>Ilimitado whatsapp BOT</li>
<li>Soporte via whatsapp</li>
<li>Backup diario</li>
                        </ul>

                                                    <a href="https://oficina.prestamoscloud.com/registro/nuevo" class="price_btn btn_hover">
                                Probar por 10 dias                            </a>
                                            </div>
                                    <div class="price_item">

                            
                            
                                                        <h5 class="f_p f_size_20 f_600 t_color2 mt_30"> Oro </h5>
                        
                        <p>Plan empresarial y cooperativas</p>

                        <div class="price f_700 f_size_40 t_color2">
                            $970                                                            <sub class="f_size_16 f_300"> / Anualmente </sub>
                                                    </div>

                        <ul class="list-unstyled p_list">
                            <li>Aplicación móvil</li>
<li>Acceso a 15 usuarios</li>
<li>(5 usd por cada usuario)</li>   
<li>Sin límite de préstamos</li>
<li>Sin límite de clientes </li>
<li>Ventas ilimitadas</li>
<li>Ilimitado whatsapp BOT</li>
<li>Soporte via whatsapp</li>
<li>Backup diario</li>
<li>Modificaciones al sistema personalizadas</li>
                        </ul>

                                                    <a href="https://oficina.prestamoscloud.com/registro/nuevo" class="price_btn btn_hover">
                                Probar por 10 dias                            </a>
                                            </div>
                            </div>
        </div>
        </div>
        </div>

        <script>
            ;(function($){
                "use strict";
                $(document).ready(function () {
                    $( '.test_slicKSlider' ).slick({
                        dots: true,
                        arrows: false,
                        autoplay: false,
                        slidesToShow: 3,
                        slidesToScroll: 1,
                        responsive: [
                            {
                                breakpoint: 1024,
                                settings: {
                                    slidesToShow: 2,
                                }
                            },
                            {
                                breakpoint: 600,
                                settings: {
                                    slidesToShow: 1,
                                }
                            }
                        ]
                    });
                })
            })(jQuery)
        </script>
        		</div>
				</div>
						</div>
			</div>
		</div>
						</div>
			</div>
		</section>
				<section class="elementor-element elementor-element-237acf96 elementor-section-height-min-height elementor-section-boxed elementor-section-height-default elementor-section-items-middle elementor-section elementor-top-section" data-id="237acf96" data-element_type="section" data-settings="{&quot;background_background&quot;:&quot;classic&quot;}">
						<div class="elementor-container elementor-column-gap-default">
				<div class="elementor-row">
				<div class="elementor-element elementor-element-59ae68a0 elementor-column elementor-col-50 elementor-top-column" data-id="59ae68a0" data-element_type="column">
			<div class="elementor-column-wrap  elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div class="elementor-element elementor-element-745a2b24 elementor-widget" data-id="745a2b24" data-element_type="widget" data-widget_type="image.default">
				<div class="elementor-widget-container">
					<div class="elementor-image">
										<img width="768" height="631" src="wp-content/uploads/2021/08/Dispositivo-768x631.png" class="attachment-medium_large size-medium_large" alt="" srcset="wp-content/uploads/2021/08/Dispositivo-768x631.png 768w, wp-content/uploads/2021/08/Dispositivo-300x246.png 300w, wp-content/uploads/2021/08/Dispositivo-1024x841.png 1024w, wp-content/uploads/2021/08/Dispositivo-85x70.png 85w, wp-content/uploads/2021/08/Dispositivo-600x493.png 600w, wp-content/uploads/2021/08/Dispositivo.png 1493w" sizes="(max-width: 768px) 100vw, 768px">											</div>
				</div>
				</div>
				<div class="elementor-element elementor-element-475e537e elementor-widget elementor-widget-text-editor" data-id="475e537e" data-element_type="widget" data-widget_type="text-editor.default">
				<div class="elementor-widget-container">
					<div class="elementor-text-editor elementor-clearfix"><div style="color: white;">
<h3><strong>Gestiona préstamos con diferentes modalidades de pago</strong></h3>
(Diarios semanales, quincenales, mensuales y diferentes tipos de amortizaciones)

</div></div>
				</div>
				</div>
				<div class="elementor-element elementor-element-4b4dfa24 elementor-align-left elementor-mobile-align-center elementor-widget elementor-widget-button" data-id="4b4dfa24" data-element_type="widget" data-widget_type="button.default">
				<div class="elementor-widget-container">
					<div class="elementor-button-wrapper">
			<a href="https://oficina.prestamoscloud.com/registro/nuevo" class="elementor-button-link elementor-button elementor-size-sm" role="button">
						<span class="elementor-button-content-wrapper">
						<span class="elementor-button-text">Solicitar demo</span>
		</span>
					</a>
		</div>
				</div>
				</div>
						</div>
			</div>
		</div>
				<div class="elementor-element elementor-element-7f33f422 elementor-column elementor-col-50 elementor-top-column" data-id="7f33f422" data-element_type="column">
			<div class="elementor-column-wrap  elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div class="elementor-element elementor-element-2342c991 elementor-widget elementor-widget-heading" data-id="2342c991" data-element_type="widget" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h2 class="elementor-heading-title elementor-size-xxl"><span style="color: yellow; font-size: 130%;">Control total</span> <p style="color:white">de tus clientes.</p></h2>		</div>
				</div>
				<div class="elementor-element elementor-element-323cc5e3 elementor-align-left elementor-mobile-align-center elementor-widget elementor-widget-button" data-id="323cc5e3" data-element_type="widget" data-widget_type="button.default">
				<div class="elementor-widget-container">
					<div class="elementor-button-wrapper">
			<a href="https://oficina.prestamoscloud.com/registro/nuevo" class="elementor-button-link elementor-button elementor-size-sm" role="button">
						<span class="elementor-button-content-wrapper">
						<span class="elementor-button-text">Crear cuenta</span>
		</span>
					</a>
		</div>
				</div>
				</div>
				<div class="elementor-element elementor-element-5566ba6c elementor-widget elementor-widget-image" data-id="5566ba6c" data-element_type="widget" data-widget_type="image.default">
				<div class="elementor-widget-container">
					<div class="elementor-image">
										<img width="768" height="880" src="wp-content/uploads/2021/08/Modelo-768x880.png" class="attachment-medium_large size-medium_large" alt="" srcset="wp-content/uploads/2021/08/Modelo-768x880.png 768w, wp-content/uploads/2021/08/Modelo-262x300.png 262w, wp-content/uploads/2021/08/Modelo-350x400.png 350w, wp-content/uploads/2021/08/Modelo-600x687.png 600w, wp-content/uploads/2021/08/Modelo.png 823w" sizes="(max-width: 768px) 100vw, 768px">											</div>
				</div>
				</div>
						</div>
			</div>
		</div>
						</div>
			</div>
		</section>
		<section style="margin-top: 12%;" class="mobile-show">
				<h4 class="elementor-heading-title elementor-size-default"  style="text-align: center;"><b>¡Descarga nuestra aplicación </b><br>Para dispositivos móviles!</span>		</h4>
				<div class="flex social-btns">
					<a class="app-btn blu flex vert" href="https://apps.apple.com/do/app/pr%C3%A9stamos-cloud/id1525391626?l=en">
						<i class="fab fa-apple"></i>
						<p>Disponible en <br/> <span class="big-txt">App Store</span></p>
					</a>

					<a class="app-btn blu flex vert" href="https://play.google.com/store/apps/details?id=prestacloudvn.cloudservices.com&hl=es&gl=US">
						<i class="fab fa-google-play"></i>
						<p>Disponible en <br/> <span class="big-txt">Google Play</span></p>
					</a>
				</div>
		</section>

				<section class="elementor-element elementor-element-63b9beab elementor-section-content-middle elementor-section-boxed elementor-section-height-default elementor-section-height-default elementor-section elementor-top-section" data-id="63b9beab" data-element_type="section">
						<div class="elementor-container elementor-column-gap-no">
				<div class="elementor-row">
				<div class="elementor-element elementor-element-4e00e343 elementor-column elementor-col-100 elementor-top-column" data-id="4e00e343" data-element_type="column">
			<div class="elementor-column-wrap  elementor-element-populated">
					<div class="elementor-widget-wrap">
				<div class="elementor-element elementor-element-639dec60 elementor-widget elementor-widget-heading" data-id="639dec60" data-element_type="widget" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h4 class="elementor-heading-title elementor-size-default"></h4><h2 data-elementor-setting-key="title" data-pen-placeholder="Teclea aquí..." style="font-family: Poppins, sans-serif;">Preguntas frecuentes (FAQ)</h2>		</div>
				</div>
				<div class="elementor-element elementor-element-71b13052 elementor-widget elementor-widget-heading" data-id="71b13052" data-element_type="widget" data-widget_type="heading.default">
				<div class="elementor-widget-container">
			<h5 class="elementor-heading-title elementor-size-default">Respuestas a las preguntas mas frecuentes de nuestros clientes.</h5>		</div>
				</div>
				<div class="elementor-element elementor-element-5ffafbfc elementor-widget elementor-widget-toggle" data-id="5ffafbfc" data-element_type="widget" data-widget_type="toggle.default">
				<div class="elementor-widget-container">
					<div class="elementor-toggle" role="tablist">
							<div class="elementor-toggle-item">
					<div id="elementor-tab-title-1611" class="elementor-tab-title" data-tab="1" role="tab" aria-controls="elementor-tab-content-1611">
												<span class="elementor-toggle-icon elementor-toggle-icon-right" aria-hidden="true">
															<span class="elementor-toggle-icon-closed"><i class="fas fa-caret-right"></i></span>
								<span class="elementor-toggle-icon-opened"><i class="elementor-toggle-icon-opened fas fa-caret-up"></i></span>
													</span>
												<a href="">1.- ¿Ustedes prestan dinero?</a>
					</div>
					<div id="elementor-tab-content-1611" class="elementor-tab-content elementor-clearfix" data-tab="1" role="tabpanel" aria-labelledby="elementor-tab-title-1611"><p>No prestamos dinero. Somos un sistema para comerciantes y prestamistas, que se utiliza para la gestión de ventas, préstamos, gastos, clientes, entre otras funcionalidades.</p></div>
				</div>
							<div class="elementor-toggle-item">
					<div id="elementor-tab-title-1612" class="elementor-tab-title" data-tab="2" role="tab" aria-controls="elementor-tab-content-1612">
												<span class="elementor-toggle-icon elementor-toggle-icon-right" aria-hidden="true">
															<span class="elementor-toggle-icon-closed"><i class="fas fa-caret-right"></i></span>
								<span class="elementor-toggle-icon-opened"><i class="elementor-toggle-icon-opened fas fa-caret-up"></i></span>
													</span>
												<a href="">2.- ¿Cuál es la forma de pago del sistema?</a>
					</div>
					<div id="elementor-tab-content-1612" class="elementor-tab-content elementor-clearfix" data-tab="2" role="tabpanel" aria-labelledby="elementor-tab-title-1612">Tiene la modalidad de pagar su licencia de forma mensual o puede optar por hacer pagos anuales con un 5% de descuento. Este pago le garantiza un soporte, asistencia y actualizaciones sin costo extra, que estarán siempre disponible para usted.
</div>
				</div>
							<div class="elementor-toggle-item">
					<div id="elementor-tab-title-1613" class="elementor-tab-title" data-tab="3" role="tab" aria-controls="elementor-tab-content-1613">
												<span class="elementor-toggle-icon elementor-toggle-icon-right" aria-hidden="true">
															<span class="elementor-toggle-icon-closed"><i class="fas fa-caret-right"></i></span>
								<span class="elementor-toggle-icon-opened"><i class="elementor-toggle-icon-opened fas fa-caret-up"></i></span>
													</span>
												<a href="">3.- ¿Me darán un entrenamiento del sistema?</a>
					</div>
					<div id="elementor-tab-content-1613" class="elementor-tab-content elementor-clearfix" data-tab="3" role="tabpanel" aria-labelledby="elementor-tab-title-1613">Contamos con una cuenta demo para que pueda realizar practicas experimentales. Además contamos con videos tutoriales para aprender sobre el funcionamiento, sumado a que tiene la asistencia de un representante calificado que le orientara en todo momento a como empezar a utilizar su sistema nuevo.
					</div>
				</div>
							<div class="elementor-toggle-item">
					<div id="elementor-tab-title-1614" class="elementor-tab-title" data-tab="4" role="tab" aria-controls="elementor-tab-content-1614">
												<span class="elementor-toggle-icon elementor-toggle-icon-right" aria-hidden="true">
															<span class="elementor-toggle-icon-closed"><i class="fas fa-caret-right"></i></span>
								<span class="elementor-toggle-icon-opened"><i class="elementor-toggle-icon-opened fas fa-caret-up"></i></span>
													</span>
												<a href="">4.- ¿Dónde están ubicados?</a>
					</div>
					<div id="elementor-tab-content-1614" class="elementor-tab-content elementor-clearfix" data-tab="4" role="tabpanel" aria-labelledby="elementor-tab-title-1614">Nuestras oficinas estan localizadas en la Ave 27 de febrero esq. Hilario Espertin, Plaza comercial Don Bosco 3er nivel, Santo Domingo, Rep. Dom.
</div>
				</div>
							<div class="elementor-toggle-item">
					<div id="elementor-tab-title-1615" class="elementor-tab-title" data-tab="5" role="tab" aria-controls="elementor-tab-content-1615">
												<span class="elementor-toggle-icon elementor-toggle-icon-right" aria-hidden="true">
															<span class="elementor-toggle-icon-closed"><i class="fas fa-caret-right"></i></span>
								<span class="elementor-toggle-icon-opened"><i class="elementor-toggle-icon-opened fas fa-caret-up"></i></span>
													</span>
												<a href="">5.- ¿Tengo que pagar una instalación?</a>
					</div>
					<div id="elementor-tab-content-1615" class="elementor-tab-content elementor-clearfix" data-tab="5" role="tabpanel" aria-labelledby="elementor-tab-title-1615">El sistema no lo requiere. Con su registro usted obtiene un usuario y contraseña. Puede descargar nuestra app y acceder de forma rápida y fácil sin costo de inscripción. Solo va a pagar el mes a utilizar una vez que se venza su cuenta demo en un plazo de 10 días.
</div>
				</div>
							<div class="elementor-toggle-item">
					<div id="elementor-tab-title-1616" class="elementor-tab-title" data-tab="6" role="tab" aria-controls="elementor-tab-content-1616">
												<span class="elementor-toggle-icon elementor-toggle-icon-right" aria-hidden="true">
															<span class="elementor-toggle-icon-closed"><i class="fas fa-caret-right"></i></span>
								<span class="elementor-toggle-icon-opened"><i class="elementor-toggle-icon-opened fas fa-caret-up"></i></span>
													</span>
												<a href="">6.- ¿Y si no quiero el sistema después de probarlo con la cuenta demo?</a>
					</div>
					<div id="elementor-tab-content-1616" class="elementor-tab-content elementor-clearfix" data-tab="6" role="tabpanel" aria-labelledby="elementor-tab-title-1616">La suscripción al sistema no conlleva compromisos y es sin contrato. Si no necesita seguir en el sistema solo debe solicitar su desactivación sin penalidad.
					</div>
				</div>
							<div class="elementor-toggle-item">
					<div id="elementor-tab-title-1617" class="elementor-tab-title" data-tab="7" role="tab" aria-controls="elementor-tab-content-1617">
												<span class="elementor-toggle-icon elementor-toggle-icon-right" aria-hidden="true">
															<span class="elementor-toggle-icon-closed"><i class="fas fa-caret-right"></i></span>
								<span class="elementor-toggle-icon-opened"><i class="elementor-toggle-icon-opened fas fa-caret-up"></i></span>
													</span>
												<a href="">7.- ¿El sistema viene con la impresora?</a>
					</div>
					<div id="elementor-tab-content-1617" class="elementor-tab-content elementor-clearfix" data-tab="7" role="tabpanel" aria-labelledby="elementor-tab-title-1617">La impresora no viene con el sistema. Usted accede desde su celular y, conectado a una impresora bluetooth, imprime sus recibos. Esta impresora se vende aparte y es un pago único.
</div>
				</div>
							<div class="elementor-toggle-item">
					<div id="elementor-tab-title-1618" class="elementor-tab-title" data-tab="8" role="tab" aria-controls="elementor-tab-content-1618">
												<span class="elementor-toggle-icon elementor-toggle-icon-right" aria-hidden="true">
															<span class="elementor-toggle-icon-closed"><i class="fas fa-caret-right"></i></span>
								<span class="elementor-toggle-icon-opened"><i class="elementor-toggle-icon-opened fas fa-caret-up"></i></span>
													</span>
												<a href="">8.- Si yo quiero cambiar de plan más adelante, ¿Lo puedo hacer sin penalidad?</a>
					</div>
					<div id="elementor-tab-content-1618" class="elementor-tab-content elementor-clearfix" data-tab="8" role="tabpanel" aria-labelledby="elementor-tab-title-1618">Puede hacerlo. Solo debe solicitarlo y sin ningún cargo usted pasa al siguiente plan. Debe pagar el costo del plan nuevo que haya elegido una vez cumplido el mes.
</div>
				</div>
							<div class="elementor-toggle-item">
					<div id="elementor-tab-title-1619" class="elementor-tab-title" data-tab="9" role="tab" aria-controls="elementor-tab-content-1619">
												<span class="elementor-toggle-icon elementor-toggle-icon-right" aria-hidden="true">
															<span class="elementor-toggle-icon-closed"><i class="fas fa-caret-right"></i></span>
								<span class="elementor-toggle-icon-opened"><i class="elementor-toggle-icon-opened fas fa-caret-up"></i></span>
													</span>
												<a href="">9.- Si deseo abandonar el sistema, ¿Cómo obtengo mis datos?</a>
					</div>
					<div id="elementor-tab-content-1619" class="elementor-tab-content elementor-clearfix" data-tab="9" role="tabpanel" aria-labelledby="elementor-tab-title-1619">Tiene en el sistema la modalidad de un Backup, que puede recibir diariamente en su correo electrónico.
</div>
				</div>
							<div class="elementor-toggle-item">
					<div id="elementor-tab-title-16110" class="elementor-tab-title" data-tab="10" role="tab" aria-controls="elementor-tab-content-16110">
												<span class="elementor-toggle-icon elementor-toggle-icon-right" aria-hidden="true">
															<span class="elementor-toggle-icon-closed"><i class="fas fa-caret-right"></i></span>
								<span class="elementor-toggle-icon-opened"><i class="elementor-toggle-icon-opened fas fa-caret-up"></i></span>
													</span>
												<a href="">10.- ¿Es este sistema seguro?</a>
					</div>
					<div id="elementor-tab-content-16110" class="elementor-tab-content elementor-clearfix" data-tab="10" role="tabpanel" aria-labelledby="elementor-tab-title-16110">Totalmente. Contamos con una seguridad cifrada de extremo a extremo que no le permitirá ser violada por ninguna persona. Actualmente este es de los sistemas con mejor encriptación y más seguro para los pequeñas y medianas empresas.

</div>
				</div>
							<div class="elementor-toggle-item">
					<div id="elementor-tab-title-16111" class="elementor-tab-title" data-tab="11" role="tab" aria-controls="elementor-tab-content-16111">
												<span class="elementor-toggle-icon elementor-toggle-icon-right" aria-hidden="true">
															<span class="elementor-toggle-icon-closed"><i class="fas fa-caret-right"></i></span>
								<span class="elementor-toggle-icon-opened"><i class="elementor-toggle-icon-opened fas fa-caret-up"></i></span>
													</span>
												<a href="">11.- ¿Qué formas de pago aceptan?</a>
					</div>
					<div id="elementor-tab-content-16111" class="elementor-tab-content elementor-clearfix" data-tab="11" role="tabpanel" aria-labelledby="elementor-tab-title-16111">Aceptamos transferencias bancarias a casi todos los bancos Dominicanos, sumado a que admitimos pagos con tarjeta de crédito/debito desde la comodidad de su casa y también pagos vía la plataforma de pagos PAYPAL.
</div>
				</div>
					</div>
				</div>
				</div>
						</div>
			</div>
		</div>
						</div>
			</div>
		</section>
						</div>
			</div>
		</div>
		                <footer class="new_footer_area bg_color">
                    <div class="new_footer_top">
                        <div class="container">
                            <div class="row">
                                <div id="text-1" class="widget footer-widget col-lg-6 col-md-6 widget_text">
                            <div class="f_widget about-widget pl_70">			<div class="textwidget"><p>&nbsp;</p>
<p><img class="wp-image-5135 alignleft" src="wp-content/uploads/2020/02/Logo-CloudServicesDominicana.png" alt="" width="154" height="61"></p>
<p class="f_300 f_p f_size_15 mb-0 l_height28 mt_30">Pioneros en el mercado de sistemas en la nube, CLOUD SERVICES DOMINICANA E.I.R.L. RNC: 1-32-05839-9</p>
</div>
		</div></div>                            </div>
                        </div>
                        <div class="footer_bg">
                                                            <div class="footer_bg_one"></div>
                                                                                </div>
                    </div>
                    <div class="footer_bottom">
                        <div class="container">
                            <div class="row align-items-center">
                                <div class="col-lg-6 col-sm-7">
                                    <p>PréstamosCLOUD APP ©</p>
<h6 data-pm-slice="1 1 []"><em>Av.27 de febrero, esq. Hilario Espertin no.6, Centro Comercial Don Bosco, Distrito Nacional, RD.</em></h6>
                                </div>
                                <div class="col-lg-6 col-sm-5 text-right">
                                    <p><a href="index-2.htm">Cloud Services Dominicana PHP E.I.R.L.. </a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </footer>
                
</div> <!-- Body Wrapper -->
	<script type="text/javascript">
		var c = document.body.className;
		c = c.replace(/woocommerce-no-js/, 'woocommerce-js');
		document.body.className = c;
	</script>
	<link rel='stylesheet' id='hero-event-css' href='wp-content/plugins/saasland-core/widgets/heros/event/hero-event.css?ver=5.3.8' type='text/css' media='all'>
<link rel='stylesheet' id='hero-chat-css' href='wp-content/plugins/saasland-core/widgets/heros/chat/hero-chat.css?ver=5.3.8' type='text/css' media='all'>
<link rel='stylesheet' id='chat-features-css' href='wp-content/plugins/saasland-core/assets/css/section-base-css/chat-features.css?ver=5.3.8' type='text/css' media='all'>
<script type='text/javascript' src='wp-content/plugins/saasland-core/assets/vendors/wow/wow.min.js?ver=1.1.3'></script>
<script type='text/javascript' src='wp-content/plugins/saasland-core/assets/js/appart-custom.js?ver=1.0.0'></script>
<script type='text/javascript' src='wp-content/plugins/saasland-core/assets/js/main.js?ver=1.0'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var wpcf7 = {"apiSettings":{"root":"https:\/\/prestamoscloud.com\/index.php\/wp-json\/contact-form-7\/v1","namespace":"contact-form-7\/v1"}};
/* ]]> */
</script>
<script type='text/javascript' src='wp-content/plugins/contact-form-7/includes/js/scripts.js?ver=5.1.6'></script>
<script type='text/javascript' src='recaptcha/api.js?render=6LdNh9UUAAAAAC06Suk9lrvx1ZAb-vXvo35y0ryi&#038;ver=3.0'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var mailchimp_public_data = {"site_url":"https:\/\/prestamoscloud.com","ajax_url":"https:\/\/prestamoscloud.com\/wp-admin\/admin-ajax.php"};
/* ]]> */
</script>
<script type='text/javascript' src='wp-content/plugins/mailchimp-for-woocommerce/public/js/mailchimp-woocommerce-public.min.js?ver=2.3.2'></script>
<script type='text/javascript' src='wp-content/themes/saasland/assets/js/propper.js?ver=1.0'></script>
<script type='text/javascript' src='wp-content/themes/saasland/assets/js/bootstrap.min.js?ver=4.1.2'></script>
<script type='text/javascript' src='wp-content/plugins/saasland-core/assets/vendors/sckroller/jquery.parallax-scroll.js?ver=1.0'></script>
<script type='text/javascript' src='wp-content/themes/saasland/assets/vendors/magnify-pop/jquery.magnific-popup.min.js?ver=1.1.0'></script>
<script type='text/javascript' src='wp-content/themes/saasland/assets/vendors/scroll/jquery.mCustomScrollbar.concat.min.js?ver=3.1.13'></script>
<script type='text/javascript'>
/* <![CDATA[ */
var local_strings = {"ajax_url":"https:\/\/prestamoscloud.com\/wp-admin\/admin-ajax.php"};
/* ]]> */
</script>
<script type='text/javascript' src='wp-content/themes/saasland/assets/js/custom-wp.js?ver=1.0'></script>
<script type='text/javascript'>
        //<![CDATA[
            jQuery(window).on( 'load', function() { // makes sure the whole site is loaded 
                jQuery( '#status' ).fadeOut(); // will first fade out the loading animation 
                jQuery( '#preloader' ).delay(350).fadeOut( 'slow' ); // will fade out the white DIV that covers the website. 
                jQuery( 'body' ).delay(350).css({'overflow':'visible'});
            })
        //]]>

        ;(function($){
            $(document).ready(function () {
                $( '.widget_search' ).removeClass( 'widget_search' ).addClass( 'search_widget_two' );
            });
        })(jQuery);
</script>
<script type='text/javascript'>
/* <![CDATA[ */
var fcaPcEvents = [{"triggerType":"post","trigger":["all"],"parameters":{"content_name":"{post_title}","content_type":"product","content_ids":"{post_id}"},"event":"ViewContent","delay":"0","scroll":"0","apiAction":"track","ID":"1416989a-fe4a-4c0e-a367-6f25827b35ea"}];
var fcaPcDebug = {"debug":""};
var fcaPcPost = {"title":"web-2","type":"page","id":"5309","categories":[],"utm_support":"","user_parameters":"","edd_delay":"0","woo_delay":"0","edd_enabled":"","woo_enabled":""};
/* ]]> */
</script>
<script type='text/javascript' src='wp-content/plugins/facebook-conversion-pixel/pixel-cat.min.js?ver=2.4.2'></script>
<script type='text/javascript' src='wp-includes/js/wp-embed.min.js?ver=5.3.8'></script>
<script type='text/javascript' src='wp-content/plugins/saasland-core/assets/js/appart-parallax.js?ver=1.0'></script>
<script type='text/javascript' src='wp-content/plugins/saasland-core/assets/vendors/slick/slick.min.js?ver=1.9.0'></script>
<script type='text/javascript' src='wp-content/plugins/elementor/assets/js/frontend-modules.min.js?ver=2.8.5'></script>
<script type='text/javascript' src='wp-includes/js/jquery/ui/position.min.js?ver=1.11.4'></script>
<script type='text/javascript' src='wp-content/plugins/elementor/assets/lib/dialog/dialog.min.js?ver=4.7.3'></script>
<script type='text/javascript' src='wp-content/plugins/elementor/assets/lib/waypoints/waypoints.min.js?ver=4.0.2'></script>
<script type='text/javascript' src='wp-content/plugins/elementor/assets/lib/swiper/swiper.min.js?ver=4.4.6'></script>
<script type='text/javascript'>
var elementorFrontendConfig = {"environmentMode":{"edit":false,"wpPreview":false},"is_rtl":false,"breakpoints":{"xs":0,"sm":480,"md":768,"lg":1025,"xl":1440,"xxl":1600},"version":"2.8.5","urls":{"assets":"https:\/\/prestamoscloud.com\/wp-content\/plugins\/elementor\/assets\/"},"settings":{"page":[],"general":[],"editorPreferences":[]},"post":{"id":5309,"title":"web-2","excerpt":""}};
</script>
<script type='text/javascript' src='wp-content/plugins/elementor/assets/js/frontend.min.js?ver=2.8.5'></script>
<script type="text/javascript">
( function( grecaptcha, sitekey, actions ) {

	var wpcf7recaptcha = {

		execute: function( action ) {
			grecaptcha.execute(
				sitekey,
				{ action: action }
			).then( function( token ) {
				var forms = document.getElementsByTagName( 'form' );

				for ( var i = 0; i < forms.length; i++ ) {
					var fields = forms[ i ].getElementsByTagName( 'input' );

					for ( var j = 0; j < fields.length; j++ ) {
						var field = fields[ j ];

						if ( 'g-recaptcha-response' === field.getAttribute( 'name' ) ) {
							field.setAttribute( 'value', token );
							break;
						}
					}
				}
			} );
		},

		executeOnHomepage: function() {
			wpcf7recaptcha.execute( actions[ 'homepage' ] );
		},

		executeOnContactform: function() {
			wpcf7recaptcha.execute( actions[ 'contactform' ] );
		},

	};

	grecaptcha.ready(
		wpcf7recaptcha.executeOnHomepage
	);

	document.addEventListener( 'change',
		wpcf7recaptcha.executeOnContactform, false
	);

	document.addEventListener( 'wpcf7submit',
		wpcf7recaptcha.executeOnHomepage, false
	);

} )(
	grecaptcha,
	'6LdNh9UUAAAAAC06Suk9lrvx1ZAb-vXvo35y0ryi',
	{"homepage":"homepage","contactform":"contactform"}
);
</script>
</body>
<script src="1_72_0/static/js/render.6ab4a7e78b5bab99546c.js" async=""></script>
</html>